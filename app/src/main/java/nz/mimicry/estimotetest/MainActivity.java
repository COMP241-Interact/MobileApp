package nz.mimicry.estimotetest;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.LinearLayout;

import com.estimote.coresdk.common.requirements.SystemRequirementsChecker;
import com.estimote.coresdk.repackaged.gson_v2_3_1.com.google.gson.Gson;
import com.estimote.coresdk.repackaged.gson_v2_3_1.com.google.gson.GsonBuilder;

public class MainActivity extends AppCompatActivity {

    protected EstimoteApplication app;
    //Create a public list of tables
    public TableList tables = new TableList();
    public Bitmap b;
    public Canvas canvas;

    //Create a GSON object to read the JSON file at the API
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.setFieldNamingPolicy(fieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
    Gson gson = gsonBuilder.create();
    AsyncHttpClient client = new AsyncHttpClient();
    //Use the webserver API for beacons to get information
    client.get("http://interact-comp241.rhcloud.com/api/beacons",
            new TextHttpResonspeHandler(){
        @Override
        public void onSuccess(int statusCode, Header[] headers, String response){
            Gson gson = new GsonBuilder().create();
            //Create a table object from the information
            Table table = gson.fromJson(response, Table.class);
            //add it to the list of Tables
            tables.add(table);

        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        app = (EstimoteApplication)this.getApplicationContext();
        //Create a bitmap to be able to draw upon and use it as a Canvas Object
        b = Bitmap.createBitmap(480, 800, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(b);
        //Set the linearLayout XML object to be drawable
        LinearLayout l = (LinearLayout) findViewById(R.id.rect);
        l.setBackgroundDrawable(new BitmapDrawable(b));
        //Draw the tables on the bitmap
        tables.drawTables(canvas);

    }

    @Override
    protected void onResume() {
        super.onResume();

        SystemRequirementsChecker.checkWithDefaultDialogs(this);
    }

    //Method is run when there is a touch on the screen
    //Gets the coordinates of the touch and uses that to find a table at that position
    //If there is a table, then draw an outline on that table
    @Override
    public boolean onTouchEvent(MotionEvent event){
        float touchX = event.getX();
        float touchY = event.getY();
        //Find the table
        Table touchedTable = tables.findTable(touchX, touchY);
        //If there is a table found then paint outline
        if(touchedTable!=null) {
            Paint outline = new Paint();
            outline.setColor(Color.BLUE);
            canvas.drawRect(touchedTable, outline);
        }
        return true;
    }
}

class Beacon
{
    int ID;
    int region;
    String name;
    int x;
    int y;
    int state;
}