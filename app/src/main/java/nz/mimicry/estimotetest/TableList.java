package nz.mimicry.estimotetest;


import android.graphics.Canvas;

public class TableList {
    //inner class to hold the table object
    private class Node{
        private Table table;
        private Node next;
        public Node(Table add){
            table = add;
            next = null;
        }
    }
    private Node head;
    //Constructor of the class, head set to null by default
    public TableList(){
        head = null;
    }

    //adds a Table object to the list
    public void add(Table table){
        Node temp = head;
        //if the head of the list is null, add the new Table to head
        if(head == null) {
            head = new Node(table);
            return;
        }
        //else go through list until the end and add Table there
        while(temp!=null) temp = temp.next;
        temp = new Node(table);
    }
    //Finding the length of the list
    //Increment result by 1 if there is still an element in the list
    public int length(){
        int result = 1;
        Node temp = head;
        while(temp!=null){
            result++;
            temp = temp.next;
        }
        return result;
    }
    //Use the draw method of the table class to draw individual tables in the list
    public void drawTables(Canvas canvas){
        Node temp = head;
        //go through list
        while(temp!=null) {
            //and draw the current table
            temp.table.draw(canvas);
            temp = temp.next;
        }
    }
    //Finds a table in the list based on the coordinates given
    public Table findTable(float x, float y){
        Node temp = head;
        //goes through the list
        while(temp!=null){
            float tableX = (float)temp.table.getX_();
            float tableY = (float)temp.table.getY_();
            //If the coordinates are within the table bounds then return that table
            if(tableX < x && x <tableX+50 && tableY < y && y<tableY+50){
                return temp.table;
            }
            //otherwise keep searching
            temp=temp.next;
        }
        //return null if not found a table
        return null;
    }
}
