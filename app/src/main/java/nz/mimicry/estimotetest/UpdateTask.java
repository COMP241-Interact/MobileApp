package nz.mimicry.estimotetest;

import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

// Simple background task to perform an HTTP request and ignore any response
public class UpdateTask extends AsyncTask<String, String, String> {

    @Override
    protected String doInBackground(String... uri)
    {

        try
        {
            // Connect to the given URL
            URL url = new URL(uri[0]);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            InputStream in = conn.getInputStream();

            InputStreamReader isw = new InputStreamReader(in);

            // Effectively ignore the result and the connection will automatically close
            int data = isw.read();
            while (data != -1) {
                char current = (char) data;
                data = isw.read();
                System.out.print(current);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }
}
