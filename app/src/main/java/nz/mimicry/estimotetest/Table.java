package nz.mimicry.estimotetest;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;


public class Table {
    //Private member variables to match the JSON File on the server
    private int id_, region_;
    private String name_;
    private int x_, y_, state_;

    //Private member variables to draw a table in a square shape
    private Paint fill;
    private Rect table;

    //Constructor for the Table class
    //Takes all the variables required in the JSON file on the server
    //Maps to local member variables
    public Table(int id,int region, String name, int x, int y, int state){
        id_ = id;
        region_ = region;
        name_ = name;
        x_ = x;
        y_ = y;
        state_ = state;
        table.set(x_, y_, x_+50, y_+50);
    }
    //Getters and Setters for the private variables

    public int getId_() {return id_;}
    public void setId_(int newID) {this.id_ = newID;}

    public int getRegion_() {return region_;}
    public void setRegion_(int newRegion) {this.region_ = newRegion;}

    public String getName_() {return name_;}
    public void setName_(String newName){this.name_ = newName;}

    public int getX_(){return x_;}
    public void setX_(int newX){this.x_ = newX;}

    public int getY_(){return y_;}
    public void setY_(int newY){this.y_ = newY;}

    public int getState_(){return state_;}
    public void setState_(int newState){this.state_ = newState;}

    //Draws the table based on its x, y coordinates
    //uses the state to change the colour of the table
    //to yellow if reserved, red it occupied
    public void draw(Canvas canvas){
        Paint stroke = new Paint();
        Paint fill = new Paint();
        stroke.setColor(Color.BLACK);
        stroke.setStyle(Paint.Style.STROKE);
        stroke.setStrokeWidth(5);
        Paint textPaint = new Paint();
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(14);
        //If its available, draw table normally
        if(state_ == 0) {
            canvas.drawRect(table, stroke);
        }
        //If it is reserved, draw as yellow with an R
        else if(state_ == 1){
            fill.setColor(Color.YELLOW);
            canvas.drawRect(table, fill);
            canvas.drawRect(table, stroke);
            canvas.drawText("R", table.exactCenterX(), table.exactCenterY(), textPaint);
        }
        //Else the table is taken, draw as red with a T
        else{
            fill.setColor(Color.RED);
            canvas.drawRect(table, fill);
            canvas.drawRect(table, stroke);
            canvas.drawText("T", table.exactCenterX(), table.exactCenterY(), textPaint);
        }
    }
}
