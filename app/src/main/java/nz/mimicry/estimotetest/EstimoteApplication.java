package nz.mimicry.estimotetest;

import android.app.Application;
import android.os.RemoteException;
import android.provider.Settings;
import android.util.Log;

import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.Region;

import java.net.URLEncoder;
import java.util.HashMap;

// The application service that can run in the background
public class EstimoteApplication extends Application implements BeaconConsumer {
    // The address of the server to communicate with
    public final String API = "http://interact-comp241.rhcloud.com/api";
    // String used to group debug information
    private final String TAG = "EstimoteApplication";

    // The BeaconManager from the ATLbeacons library
    private BeaconManager beaconManager;

    // The devices unique identifier
    private String identifier;

    @Override
    public void onCreate() {
        super.onCreate();

        beaconManager = BeaconManager.getInstanceForApplication(this);

        // Configure the beaccon manager for quick response to beacon events
        beaconManager.setRegionExitPeriod(2500);
        beaconManager.setBackgroundScanPeriod(1000);
        beaconManager.setBackgroundBetweenScanPeriod(200);
        // Scan for eddystone beacons
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_UID_LAYOUT));

        // Set the unique device identifier
        identifier = URLEncoder.encode(Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));

        beaconManager.bind(this);
    }

    // DEBUG: will kill the beacon scanning when the app is closed (unwanted)
    // However, this will prevent multiple beacon events firing when reloading the app through android studio
    public void Dest()
    {
        Log.i(TAG, "Destroyed");
        beaconManager.unbind(this);
    }

    @Override
    public void onBeaconServiceConnect() {
        // For now hardcode the beacons we are looking for. In the furture thin information could be retrieved from a server
        HashMap<Integer, String> beacons = new HashMap<Integer, String>();
        beacons.put(1, "746dfb5ab728"); // blueberry
        beacons.put(2, "f58140b008b3"); // ice
        beacons.put(3, "9888141a475b"); // mint
        beacons.put(4, "8be1168c30fd"); // lemon
        beacons.put(5, "269b1e461c1d"); // candy
        beacons.put(6, "24604344603e"); // beetroot
        beacons.put(7, "4e79572f79e9"); // lemon(2)
        beacons.put(8, "9a6c79a2731e"); // candy(2)
        beacons.put(9, "9983b893eefa"); // beetroot(2)

        // Bind events to beacons
        beaconManager.addMonitorNotifier(new MonitorNotifier() {
            @Override
            public void didEnterRegion(Region region) {
                Log.i(TAG, "Enter Region: " + region.getId2());
                // Send info about beacon that the device is near to the server
                new UpdateTask().execute(API + "/enter/" + identifier + "/" + URLEncoder.encode(region.getUniqueId()));
            }

            @Override
            public void didExitRegion(Region region) {
                Log.i(TAG, "Exit Region:" + region.getId2());
                // Let the server know the device has left range of this beacon
                new UpdateTask().execute(API + "/leave/" + identifier + "/" + URLEncoder.encode(region.getUniqueId()));
            }

            @Override
            public void didDetermineStateForRegion(int state, Region region) {
                Log.i(TAG, "I have just switched from seeing/not seeing beacons: "+state);
            }
        });

        try {
            // Start tracking all beacons in the hashmap created above;
            Region region;
            for (int key :
                    beacons.keySet()) {
                region = new Region(Integer.toString(key), null, Identifier.parse(beacons.get(key)), null);
                beaconManager.startMonitoringBeaconsInRegion(region);
            }
        } catch (RemoteException e) {    }
    }
}
